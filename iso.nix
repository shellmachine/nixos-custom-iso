{lib, config, pkgs, ...}:

let
version = "0.4";
home-manager = builtins.fetchGit {
  url = "https://github.com/rycee/home-manager.git";
  rev = "63f299b3347aea183fc5088e4d6c4a193b334a41";
  ref = "release-20.09";
};
in
{
imports = [
  <nixpkgs/nixos/modules/profiles/all-hardware.nix>
  <nixpkgs/nixos/modules/installer/cd-dvd/iso-image.nix>
	(import "${home-manager}/nixos")
];

home-manager.users.armin = { pkgs, ... }: {
  home.packages = [ pkgs.htop ];
  programs.bash.enable = true;
  home.file."install".source = ./install;
  home.file."configuration.nix".source = ./configuration.nix;
  home.file.".config/xfce4/xfconf/xfce-perchannel-xml/keyboards.xml".source = ./configs/keyboards.xml;
  home.file.".config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml".source = ./configs/xsettings.xml;
  home.file.".config/xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml".source = ./configs/xfwm4.xml;
  home.file.".vimrc".source = ./configs/vimrc;
  home.file.".themes".source = ./configs/themes;
  home.file.".xsession".source = ./configs/xsession;
  home.file.".xinitrc".source = ./configs/xinitrc;
  home.file.".config/i3/config".source = ./configs/i3-config;
  home.file.".local/bin/fix-mouse-speed".source = ./configs/fix-mouse-speed;
  home.file.".config/gtk-3.0".source = ./configs/gtk-3.0;
  home.file.".config/alacritty/alacritty.yml".source = ./configs/alacritty.yml;
  home.file.".bashrc".source = ./configs/bashrc;
  home.file."configs".source = ./configs;
};

hardware.pulseaudio.enable = true;
nixpkgs.config.allowUnfree = true;

boot.kernelPackages = pkgs.linuxPackages_latest;
boot.loader.systemd-boot.enable = true;
boot.loader.systemd-boot.memtest86.enable = true;
boot.loader.systemd-boot.configurationLimit = 20;
boot.loader.efi.canTouchEfiVariables = true;
boot.initrd.kernelModules = [ "dm-snapshot" ];
boot.cleanTmpDir = true;
boot.tmpOnTmpfs = true;
boot.kernelModules = [ "dm-snapshot" ];

# disable sudo password
security.sudo.wheelNeedsPassword = false;

# set hostname
networking.hostName = "ultra";

# set timezone
time.timeZone = "Europe/Berlin";

# console colors
console.colors = [ "181a1e" "a54242" "8c9440" "de935f" "5f819d" "85678f" "5e8d87" "bbbbbb" "575b61" "cc6666" "b5bd68" "f0c674" "81a2be" "b294bb" "8abeb7" "dddddd" ];

console.keyMap = "de";

services.openssh.enable = true;
services.openssh.passwordAuthentication = true;
services.xserver.enable = true;
services.xserver.layout = "de";
services.xserver.libinput.enable = true;
services.cron.mailto = "root";
services.xserver.enableCtrlAltBackspace = true;

isoImage.isoName = "nixos-custom-${version}-${pkgs.stdenv.system}.iso";
isoImage.volumeID = lib.substring 0 11 "NIXOS_CUSTOM";
isoImage.appendToMenuLabel = " Live System";

isoImage.makeEfiBootable = true;
isoImage.makeUsbBootable = true;

isoImage.splashImage = pkgs.fetchurl {
  url = https://raw.githubusercontent.com/NixOS/nixos-artwork/5729ab16c6a5793c10a2913b5a1b3f59b91c36ee/ideas/grub-splash/grub-nixos-1.png;
  sha256 = "43fd8ad5decf6c23c87e9026170a13588c2eba249d9013cb9f888da5e2002217";
};

# Automatically login as root.
#services.xserver.desktopManager.xfce.enable = true;
services.xserver.windowManager.i3.enable = true;
#services.xserver.displayManager.lightdm.enable = true;
services.xserver.displayManager.autoLogin.enable = true;
services.xserver.displayManager.autoLogin.user = "armin";
#services.xserver.displayManager.defaultSession = "i3";
#services.xserver.displayManager.autoLogin.user = "armin";

services.gnome3.gnome-keyring.enable = true;

networking = {
  networkmanager.enable = true;
  #wireless.enable = true;
  #useDHCP = true;
  #interfaces = {};
};

powerManagement.enable = true;

users.extraUsers.root.initialHashedPassword = "";

# user account
users.users.armin = {
  uid = 1000;
  isNormalUser = true;
  createHome = true;
  home = "/home/armin";
  extraGroups = [ "wheel" "audio" "networkmanager" ];
  # mkpasswd
  hashedPassword = "$6$PUBD2.wkrjqlo$vM/1R9AnizoaR7rfVk099TkgZC.Hx9iLAA4ebDY6qFlWuco5dCpUdfYr15etOtoCFOMhswJN2iq7YVcA.DyQH0";
};

users.users.armin.openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMO+eWGsJGrSxxo9a4KDaeioUxwGTOX3RKL6smHUoLz6 armin@phoenix" ];

programs.bash.enableCompletion = true;

services.mingetty.greetingLine = lib.mkForce '' '';
services.mingetty.helpLine = lib.mkForce '' '';

environment.systemPackages = with pkgs; [
alacritty
arandr
arc-theme
audacious
autocutsel
chromium
cmus
cryptsetup
curl
dnsutils
feh
file
firefox
flameshot
fluxbox
gajim
hexchat
gcc
git
ghidra-bin
gitFull
gnome3.eog
gnome3.evince
gnupg
gptfdisk
htop
id3v2
irssi
jetbrains-mono
lxappearance
lynx
maim
mc
mkpasswd
mpv
mtr
ncdu
neofetch
neomutt
neovim
nerdfonts
networkmanager
networkmanagerapplet
newsboat
nginx
ntfs3g
openssl
papirus-icon-theme
pavucontrol
powerline-fonts
psmisc
pulseaudio
pulsemixer
pwgen
python38
python38Packages.pip
python38Packages.virtualenv
ranger
rsync
tcpdump
telnet
tmux
tree
tshark
unrar
unzip
vim
vimPlugins.vim-nix
w3m
wget
wine
wireshark
xclip
];

fonts.fonts = [ pkgs.nerdfonts pkgs.jetbrains-mono pkgs.powerline-fonts ];

programs.vim.defaultEditor = true;
environment.etc."vimrc" = {
text = pkgs.lib.mkDefault( pkgs.lib.mkAfter ''
set mouse=
syn on
set bg=dark
set et
set bs=2
set tabstop=2
set softtabstop=2
set shiftwidth=2
" This remembers where you were the last time you edited the file, and
" returns to the same position on startup
au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif
'');
};

environment.etc."tmux.conf" = {
text = pkgs.lib.mkDefault( pkgs.lib.mkAfter ''
set -g terminal-overrides 'xterm*:smcup@:rmcup@'
bind r source-file ~/.tmux.conf
unbind C-b
set -g prefix ^A
bind a send-prefix
set -g prefix ^a
unbind ^X
bind ^X lock-server
unbind x
bind x lock-server
unbind ^C
bind ^C new-window
unbind c
bind c new-window
unbind ^D
bind ^D detach
unbind *
bind * list-clients
unbind ^@
bind ^@ next-window
unbind ^N
bind ^N next-window
unbind " "
bind " " next-window
unbind n
bind n next-window
unbind A
bind A command-prompt "tmux rename-window %%"
unbind ^A
bind ^A last-window
unbind ^H
bind ^H previous-window
unbind ^P
bind ^P previous-window
unbind p
bind p previous-window
unbind BSpace
bind BSpace previous-window
unbind ^W
bind ^W list-windows
unbind w
bind w list-windows
unbind '\'
bind '\' confirm-before "kill-server"
unbind K
bind K confirm-before "kill-window"
unbind k
bind k confirm-before "kill-window"
unbind ^L
bind ^L refresh-client
unbind l
bind l refresh-client
unbind |
bind - split-window -v
unbind |
bind | split-window -h
unbind Tab
bind Tab select-pane -t:.+
unbind BTab
bind BTab select-pane -t:.-
set -g set-titles on
set-option -g set-titles-string "#S / #W"
set -g status-bg "default"
set -g status-fg "colour244"
set-option -g buffer-limit 100
set-option -g clock-mode-style 24
set-option -g display-time 1000
set-option -g default-terminal "screen-256color"
set-option -g history-limit 2000
set-option -g renumber-windows on
set-option -g set-titles on
set-option -g set-titles-string "[tmux] #T"
set -g status-justify left
set -g status-bg default
set -g status-fg colour12
set -g status-interval 2
set -g status-left ""
set-option -g visual-activity off
set-option -g visual-bell off
set-option -g visual-silence off
set-window-option -g monitor-activity off
set-option -g bell-action none
setw -g clock-mode-colour colour135
set -g status-position top
set -g status-bg black
set -g status-fg colour137
set -g status-left "[tmux] "
set -g status-right "#[fg=colour4,bg=black] [#(hostname)] #[fg=colour248,bg=black] %H:%M:%S "
set -g status-right-length 50
set -g status-left-length 20
setw -g window-status-current-format " #[fg=colour4]#I#[fg=colour5]:#[fg=colour10]#W#[fg=colour41]#F "
setw -g window-status-format " #[fg=colour4]#I#[fg=colour237]:#[fg=colour248]#W#[fg=colour142]#F "
bind h select-pane -L
bind l select-pane -R
bind j select-pane -D
#bind k select-pane -U
bind r source-file ~/.tmux.conf \; display "Configuration Reloaded!"
set -g message-style "fg=colour43,bg=colour240"
set -g pane-border-style "fg=colour239"
set -g pane-active-border-style "bg=default fg=colour239"
'');
};

system.stateVersion = "20.09";

}



