# custom configuration.nix for host "phoenix"
{ lib, config, pkgs, ... }:
let
home-manager = builtins.fetchGit {
  url = "https://github.com/rycee/home-manager.git";
  rev = "63f299b3347aea183fc5088e4d6c4a193b334a41";
  ref = "release-20.09";
};
in
{

imports = [
  ./hardware-configuration.nix
  (import "${home-manager}/nixos")
];

home-manager.users.armin = { pkgs, ... }: {
  home.packages = [ pkgs.htop ];
  programs.bash.enable = true;
  home.file.".config/xfce4/xfconf/xfce-perchannel-xml/keyboards.xml".source = ./configs/keyboards.xml;
  home.file.".config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml".source = ./configs/xsettings.xml;
  home.file.".config/xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml".source = ./configs/xfwm4.xml;
  home.file.".vimrc".source = ./configs/vimrc;
  home.file.".themes".source = ./configs/themes;
  home.file.".xsession".source = ./configs/xsession;
  home.file.".xinitrc".source = ./configs/xinitrc;
  home.file.".config/i3/config".source = ./configs/i3-config;
  home.file.".local/bin/fix-mouse-speed".source = ./configs/fix-mouse-speed;
  home.file.".config/gtk-3.0".source = ./configs/gtk-3.0;
  home.file.".config/alacritty/alacritty.yml".source = ./configs/alacritty.yml;
  # home.file.".bashrc".source = ./bashrc;
};


# boot stuff
boot.kernelPackages = pkgs.linuxPackages_latest;
boot.loader.systemd-boot.enable = true;
boot.loader.systemd-boot.memtest86.enable = true;
boot.loader.systemd-boot.configurationLimit = 20;
boot.loader.efi.canTouchEfiVariables = true;
boot.initrd.kernelModules = [ "dm-snapshot" ];
boot.initrd.luks.devices = { enc-pv = { device = "/dev/disk/by-uuid/XXLUKSUUIDXX"; }; };
boot.cleanTmpDir = true;
boot.tmpOnTmpfs = true;
boot.kernelModules = [ "dm-snapshot" ];

# Supposedly better for the SSD.
fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];

# console keymap
console.keyMap = "de";

# console colors
console.colors = [ "181a1e" "a54242" "8c9440" "de935f" "5f819d" "85678f" "5e8d87" "bbbbbb" "575b61" "cc6666" "b5bd68" "f0c674" "81a2be" "b294bb" "8abeb7" "dddddd" ];

# allow installation of non-free packages
nixpkgs.config.allowUnfree = true;

# set hostname
networking.hostName = "phoenix";

# set timezone
time.timeZone = "Europe/Berlin";

# automatic garbage collection
nix.gc.automatic = true;

# define packages
environment.systemPackages = with pkgs; [
alacritty
arandr
arc-theme
audacious
autocutsel
bat
cargo
chromium
cmus
colordiff
compton
curl
discord
feh
file
firefox
flameshot
gajim
gcc
git
gnome3.eog
gnome3.evince
home-manager
htop
i3blocks
id3v2
inetutils
irssi
jetbrains-mono
keepassxc
kitty
krita
libnotify
libreoffice
lxappearance
lynx
maim
mandelbulber
mc
mkpasswd
moc
mpv
ncdu
neofetch
neomutt
neovim
newsboat
(pkgs.lowPrio pkgs.neovim-unwrapped)
nerdfonts
networkmanager
networkmanagerapplet
nitrogen
nordic
nordic-polar
ntfs3g
openssl
papirus-icon-theme
pavucontrol
pciutils
pnmixer
polybarFull
powerline-fonts
pulseaudio
pulsemixer
psmisc
python38
python38Packages.virtualenv
python38Packages.pip
ranger
element-desktop
rofi
rustc
spotify
tdesktop
thunderbird
tmux
tree
typespeed
unrar
unzip
variety
vim
vimPlugins.vim-nix
weechat
wget
wireshark
xarchiver
xclip
zsh
zsh-completions
zsh-powerlevel10k
];

fonts.fonts = [ pkgs.nerdfonts pkgs.jetbrains-mono pkgs.powerline-fonts ];

# enable network manager
networking = {
  networkmanager.enable = true;
  #wireless.enable = true;
  #useDHCP = true;
  #interfaces = {};
};

# default editor
programs.vim.defaultEditor = true;

# pulseaudio
hardware.pulseaudio.enable = true;

# ssh
services.openssh.enable = true;
services.openssh.passwordAuthentication = true;

# xorg
services.xserver.enable = true;
services.xserver.windowManager.i3.enable = true;
services.xserver.windowManager.i3.package = pkgs.i3-gaps;
services.xserver.displayManager.startx.enable = true;
services.xserver.desktopManager.xfce.enable = true;
services.xserver.libinput.enable = true;
services.xserver.enableCtrlAltBackspace = true;
# dpi
#services.xserver.dpi
services.xserver.layout = "de";

# cron
services.cron.mailto = "root";

# user
users.users.armin = {
	uid = 1000;
	isNormalUser = true;
	createHome = true;
	home = "/home/armin";
	extraGroups = [ "wheel" "audio" "networkmanager" ];
	hashedPassword = "$6$6IzmHWGNmFVNSH$CUz/G6xUI.x0Qu0zZuFn954jB9cTpytUwylbgnEM.xh6ttV5kkp6K092Elnj72q/aGX6wndjAZQSK40tpnJQt/";
};
users.users.armin.openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIqcx6tcZ0Ul2v/tZOiDnWWUz92whJheZjGZsfu6oZVW" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFj35kid+2tD/qF+A6GNW66BJpOxAeRgsKzG27oBUNIn" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGp/OEF38398sgeX672oqsB7U+wlLw45louhQNp9YGTQ" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBZL1zGZhpiglQMD1PX7mViW4BTASeEuHu6Uaskt/IWb" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIH6n24XJoBc+5yP7Ua1yqzhobXgP/+dDPDKEW5WgwguA" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKaerTZVd2QZ33AqhQhF1ywn+wOZqfq9yeMPayvGji1j" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJFE5HopPFQM8M7EjhKezCEdnXcVJiXFck2bNbwC2Dz9" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC4GAdJdDXtt2jQH6b/f0e34oVGSFKmF2yFgTZTajlxQ" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMQ6CpoIeIwRBZ76kHtYwsEeqjqxFi0lIy1yagHV7WM1" "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKAYVaglejbDrqEHeWxU1DLk/afNTHGjicSvgzCaiFio" ];

# completions
programs.bash.enableCompletion = true;
programs.zsh.enableCompletion = true;

# enlarge histsize on zsh
programs.zsh.histSize = 15000;

# disable sudo password
security.sudo.wheelNeedsPassword = false;

# enable ssh-agent
programs.ssh.startAgent = true;

# tty greeting
services.mingetty.greetingLine = lib.mkForce ''\n - NixOS ${config.system.nixos.label} (\m) [\l] '';

# help line
services.mingetty.helpLine = lib.mkForce "";

# lid close action
#services.logind.lidSwitch = ignore;
# lid close action (docked)
#services.logind.lidSwitchDocked

# stateversion
system.stateVersion = "20.09";


}


